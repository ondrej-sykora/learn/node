# Node.js

Node.js® is a JavaScript runtime built on Chrome's [V8 JavaScript engine](https://developers.google.com/v8/). Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. Node.js' package ecosystem, [npm](https://www.npmjs.com/), is the largest ecosystem of open source libraries in the world.

## How to install Node JS

1. Download the last version from [Node JS](https://nodejs.org/en/) website
1. Install package

To check if you have Node.js installed, run this command in your terminal.

```cmd
> node -v
```

You can download the any version of Node JS from the [download](https://nodejs.org/en/download/) page.

# npm (Node Package Manager)

npm is the package manager for Node.js. It was created in 2009 as an open source project to help JavaScript developers easily share packaged modules of code.

## How to isntall npm

npm is distributed with Node.js- which means that when you download Node.js, you automatically get npm installed on your computer.

To confirm that you have npm installed you can run this command in your terminal.

```cmd
> npm -v
```

# Yarn

Yarn is a package manager for your code. It allows you to use and share code with other developers from around the world. Yarn does this quickly, securely, and reliably so you don’t ever have to worry.

## How to install Yarn

Before you start using Yarn, you'll first need to install it on your system. On the [Yarn](https://yarnpkg.com/lang/en/docs/install/) web is a growing number of different ways to install Yarn.

Test that Yarn is installed by running.

```cmd
> yarn -v
```

# Yarn vs npm

Yarn is a new JavaScript package manager built by Facebook, Google, Exponent and Tilde. As can be read in the official announcement, its purpose is to solve a handful of problems that these teams faced with npm, namely:

* installing packages wasn’t fast/consistent enough, and
* there were security concerns, as npm allows packages to run code on installation.

You can read more informations about Yarn vs npm [here](https://www.sitepoint.com/yarn-vs-npm/).